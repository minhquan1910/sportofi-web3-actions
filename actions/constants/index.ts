import rpc from "./rpc";
import {Bet2WinABI, AuthorityABI, GTokenABI, TreasuryABI} from "./abi";
import {Bet2WinAddress, AuthorityAddress, GTokenAddress, TreasuryAddress} from "./address";
import match_status from "./match_statuses";
import games from "./games";
const CURRENT_NETWORK = "testnet" // "mainnet | testnet"

export default {
    BET_TO_WIN_ABI: JSON.parse(Bet2WinABI),
    BET_TO_WIN_ADDRESS: Bet2WinAddress[CURRENT_NETWORK],
    AUTHORITY_ABI: JSON.parse(AuthorityABI), 
    AUTHORITY_ADDRESS: AuthorityAddress[CURRENT_NETWORK],
    GTOKEN_ABI: JSON.parse(GTokenABI),
    GTOKEN_ADDRESS: GTokenAddress[CURRENT_NETWORK],
    TREASURY_ABI: JSON.parse(TreasuryABI),
    TREASURY_ADDRESS: TreasuryAddress[CURRENT_NETWORK],
    RPC_URL : rpc[CURRENT_NETWORK],
    apiConfigs : {
        DOMAIN_NAME: "https://api-football-v1.p.rapidapi.com/v3",
        endpoints: {
            FIXTURES: "/fixtures",
        },
        headers: {
            X_RAPID_API_HOST: "X-RapidAPI-Host",
            X_RAPID_API_KEY: "X-RapidAPI-Key"
        },
        method:{
            GET: "get",
            POST: "post",
        },
        API_HOST: "api-football-v1.p.rapidapi.com",
    },
    sides: {
        HOME: 1,
        DRAW: 2,
        AWAY: 3,
    },
    settleStatus: {
        FIRST_HALF: 1,
        SECOND_HALF: 2,
        FULL_MATCH: 3,
    },
    matchStatuses: match_status,
    TIME_END: 100,
    game: games,
    storage: {
        periodic: {
            EVERY_5_MINUTES: "EVERY_5_MINUTES",
            DAILY: "DAILY",
        },
        key: {
            MATCH_IDS: "MATCH_ID",
        }
    }, 
    apiConfig: {
        DOMAIN: "https://alpha.sportofi.com/",
        SUB_DOMAIN: ".netlify/functions/",
        endpoints: {
            SAVE_HISTORY: "saveHistory-background",
            GET_HISTORY: "getHistory",
            UPDATE_HISTORY: "updateHistory-background",
        }
    },
}