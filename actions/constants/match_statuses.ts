import {Status} from "../interfaces";
const statuses = {
    TIME_TO_BE_DEFINED: {
        idx: 0,
        code: "TBD",
        name: "TIME_TO_BE_DEFINED"
    } as Status,
    NOT_STARTED: {
        idx: 1,
        code: "NS",
        name: "NOT_STARTED"
    } as Status,
    FIRST_HALF: {
        idx: 2,
        code: "1H",
        name: "First Half",
        description: "Kick off"
    } as Status,
    HALF_TIME: {
        idx: 3,
        code: "HT",
        name: "Half Time"
    },
    SECOND_HALF: {
        idx: 4,
        code: "2H",
        name: "Second Half",
        description: "2nd Half Started"
    },
    EXTRA_TIME: {
        idx: 5,
        code: "ET",
        name: "Extra Time"
    },
    PENALTY_IN_PROGRESS: {
        idx: 6,
        code: "P",
        name: "Penalty In Progress"
    },
    MATCH_FINISHED: {
        idx: 7,
        code: "FT",
        name: "Match Finished"
    },
    MATCH_FINISHED_AFTER_EXTRA_TIME: {
        idx: 8,
        code: "AET",
        name: "Match Finished After Extra Time"
    },
    MATCH_FINISHED_AFTER_PENALTY: {
        idx: 9,
        code: "PEN",
        name: "Match Finished After Penalty"
    },
    BREAK_TIME: {
        idx: 10,
        code: "BT",
        name: "Break Time"
    },
    SUSPENDED: {
        idx: 11,
        code: "SUSP",
        name: "Match Suspended"
    },
    INTERRUPT: {
        idx: 12,
        code: "INT",
        name: "Match Interrupted"
    },
    POSTPONED: {
        idx: 13,
        code: "PST",
        name: "Match Postponed"
    },
    CANCELED: {
        idx: 14,
        code: "CANC",
        name: "Match Canceled"
    },
    ABANDONED: {
        idx: 15,
        code: "ABD",
        name: "Match Abandoned"
    },
    TECHNICALLY_LOST: {
        idx: 16,
        code: "AWD",
        name: "Match Technically Lost"
    },
    WALK_OVER: {
        idx: 17,
        code: "WO",
        name: "Match Walk Over"
    },
    LIVE: {
        idx: 18,
        code: "LIVE",
        name: "IN PROGRESS"
        
    }
}

export default statuses;