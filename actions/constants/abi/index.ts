import AuthorityABI from "./AuthorityABI";
import Bet2WinABI from "./Bet2WinABI";
import GTokenABI from "./GTokenABI";
import TreasuryABI from "./TreasuryABI";
export {
    AuthorityABI,
    Bet2WinABI,
    GTokenABI,
    TreasuryABI
}