import AuthorityAddress from './AuthorityAddress';
import Bet2WinAddress from './Bet2WinAddress';
import GTokenAddress from './GtokenAddress';
import TreasuryAddress from './TreasuryAddress';
export {
    AuthorityAddress,
    Bet2WinAddress,
    GTokenAddress,
    TreasuryAddress
}