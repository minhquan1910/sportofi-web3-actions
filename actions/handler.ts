import { ActionFn, Context, Event, TransactionEvent, WebhookEvent } from "@tenderly/actions";
import { BigNumber, ethers } from "ethers";
import constants from "./constants";
import { Match } from "./interfaces";
import { MatchImpl, StatusCodeImpl } from "./implemetations";
import { isEmpty, join, difference, toString } from "lodash";
import axios from "axios";
/*
  TransactionEvent:
  Triggered when BetPlaced Event was emitted.
  Recievie Betplaced event (user: address, id: uint256, side: uint48, settleStatus: uinit48, odd: uinit48)
  Call API with matchId = id to fetch match data
  Re-call after an specific interval until match status is finished
  Call match status is finished, call resolveMatch(gameId_: uint256, matchId_: uint256, status_: uint256, sideInFavor_: uint256) by croupier
*/
const resolveMatch = async (
    croupierPrivateKey: string,
    matchId: BigNumber,
    status_idx: BigNumber,
    sideInFavor: BigNumber
) => {
    const provider = new ethers.providers.JsonRpcProvider(constants.RPC_URL);
    const croupierWallet = new ethers.Wallet(croupierPrivateKey, provider);
    const bettingContract = new ethers.Contract(
        constants.BET_TO_WIN_ADDRESS,
        constants.BET_TO_WIN_ABI,
        croupierWallet
    );
    const gameId = constants.game.SCORE.code;
    await bettingContract.resolveMatch(
        gameId,
        matchId,
        status_idx,
        sideInFavor
    );
};
const handleMatchFinished = async (
    match: Match,
    croupierPrivateKey: string
) => {
    console.log("Match", {
        matchId: match.id,
        status: match.status,
        score: match.score,
    })
    const sideInFavor = MatchImpl.getSideInFavor(match);
    console.log("sideInFavor", sideInFavor);
    if (sideInFavor) {
        await resolveMatch(
            croupierPrivateKey,
            BigNumber.from(match.id),
            BigNumber.from(match?.status?.idx),
            BigNumber.from(sideInFavor)
        );
    }
};

const getMatchIdList = async (context: Context, periodic: string) => {
    const key: string = `${periodic}/${constants.storage.key.MATCH_IDS}`;
    const matchIdsJson = await context.storage.getJson(key);
    console.log("matchIdsJson", matchIdsJson);
    if (isEmpty(matchIdsJson)) {
        return [];
    } else {
        return JSON.parse(matchIdsJson);
    }
};
const putToStorage = async (
    context: Context,
    matchId: number,
    periodic: string
) => {
    console.log(`Put matchId ${matchId} to storage with periodic ${periodic}`);
    const matchIds = await getMatchIdList(context, periodic);
    if (isEmpty(matchIds)) {
        console.log("Array is empty");
        const newMatchIdsString = JSON.stringify([matchId]);
        const key: string = `${periodic}/${constants.storage.key.MATCH_IDS}`;
        await context.storage.putJson(key, newMatchIdsString);
    } else {
        if (!matchIds.includes(matchId)) {
            console.log(`Add ${matchId} to array`);
            matchIds.push(matchId);
            console.log("matchIds", matchIds);
            const newMatchIdsString = JSON.stringify(matchIds);
            const key: string = `${periodic}/${constants.storage.key.MATCH_IDS}`;
            await context.storage.putJson(key, newMatchIdsString);
        } else {
            console.log(`${matchId} is already in array`);
        }
    }
};
const removeFromStorage = async (
    context: Context,
    matchIdArr: number[],
    periodic: string
) => {
    console.log(
        `Remove matchIds ${matchIdArr} from storage with periodic ${periodic}`
    );
    const matchIds = await getMatchIdList(context, periodic);
    const remainIds = difference(matchIds, matchIdArr);
    const newMatchIdsString = JSON.stringify(remainIds);
    const key: string = `${periodic}/${constants.storage.key.MATCH_IDS}`;
    await context.storage.putJson(key, newMatchIdsString);
};

const betPlacedAction: ActionFn = async (context: Context, event: Event) => {
    let txEvent = event as TransactionEvent;
    const iface = new ethers.utils.Interface(constants.BET_TO_WIN_ABI);
    const length = txEvent.logs.length;
    const eventDecoded = iface.decodeEventLog(
        "BetPlaced",
        txEvent.logs[length - 1].data,
        txEvent.logs[length - 1].topics
    );
    const { user, id, side, settleStatus, odd, amount } = eventDecoded;
    console.log("Event Emitted", {
        user,
        id: BigInt.asUintN(48, id),
        side: toString(side),
        settleStatus: toString(settleStatus),
        amount: toString(amount),
        odd: toString(odd),
    });
    const croupierPK = await context.secrets.get("CROUPIER_PK");
    const matchId = Number(BigInt.asUintN(48, id));
    console.log("matchId", matchId);
    const apiKey = await context.secrets.get("API_KEY");

    let match: Match = await MatchImpl.getMatchById(matchId, apiKey);
    if (!isEmpty(match)) {
        if (
           MatchImpl.isMatchFinished(match?.status?.code) 
        ) {
            console.log(`Match ${matchId} is already finished`);
            await handleMatchFinished(match, croupierPK);
        } else {
            let periodic = StatusCodeImpl.getPeriodic(match);
            console.log(`Match ${matchId} is not finished`);
            await putToStorage(context, matchId, periodic);
        }
    }
};
const checkMatchHandler = async (context: Context, periodic: string) => {
    console.log("Checkmatch Handler");
    const matchIds = await getMatchIdList(context, periodic);
    console.log("matchIds", matchIds);
    const apiKey = await context.secrets.get("API_KEY");
    const matchIdString = join(matchIds, "-");
    console.log(`matchIdString ${matchIdString}`);
    const allMatches: Match[] = await MatchImpl.getMatchByIds(
        matchIdString,
        apiKey
    );
    const finishedMatches = allMatches.filter(
        (match) => MatchImpl.isMatchFinished(match?.status?.code)
    );
    const finishedMatchIds = finishedMatches
        .map((match: Match) => match.id)
        .filter((id) => id !== undefined) as number[];
    const unFinishedMatches = allMatches.filter(
        (match) => !MatchImpl.isMatchFinished(match?.status?.code)
    );
    console.log("finishedMatchIds", finishedMatchIds);
    if (!isEmpty(finishedMatchIds)) {
        const accessToken = await context.secrets.get("ACCESS_TOKEN"); 
        await removeFromStorage(context, finishedMatchIds, periodic);
        for (let match of finishedMatches) { 
            // delete not needed properties
            delete match.events;
            delete match.lineups;
            delete match.statistics;
            delete match.players;
            const options = {
                method: 'post',
                url: 'https://api.tenderly.co/api/v1/actions/c44e36fe-250b-497a-ae55-f7dd723c88a5/webhook',
                headers: {
                    'x-access-key': accessToken,
                    'Content-Type': 'application/json'
                },
                data: {
                    matchData: match
                }
            }
            console.log("options: ", options);
            await axios(options);
        }
        for (let match of unFinishedMatches) {
            let newPeriodic = StatusCodeImpl.getPeriodic(match);
            if(!isEmpty(match) && newPeriodic!==periodic){
                await removeFromStorage(context, [match.id as number], periodic);
                await putToStorage(context, match.id as number, newPeriodic);
            }
        }

    }
};
const checkMatchFinishedAction: ActionFn = async (
    context: Context,
    event: Event
) => {
    const periodic = constants.storage.periodic.EVERY_5_MINUTES;
    await checkMatchHandler(context, periodic);
};
const reCheckMatchAction: ActionFn = async (context: Context, event: Event) => {
    const periodic = constants.storage.periodic.DAILY;
    await checkMatchHandler(context, periodic);
};
const saveBetPlacedHistoryAction: ActionFn = async (
    context: Context,
    event: Event
) => {
    let txEvent = event as TransactionEvent;
    const iface = new ethers.utils.Interface(constants.BET_TO_WIN_ABI);
    const length = txEvent.logs.length;
    const eventDecoded = iface.decodeEventLog(
        "BetPlaced",
        txEvent.logs[length - 1].data,
        txEvent.logs[length - 1].topics
    );
    const { user, id, side, settleStatus, odd, amount } = eventDecoded;
    const _amount = ethers.utils.formatEther(amount);
    console.log("Event Emitted", {
        user,
        id: toString(id),
        side: toString(side),
        amount: _amount,
        settleStatus: toString(settleStatus),
        odd: toString(odd),
    });
    const encodedId = ethers.BigNumber.from(id);
    console.log("encodedId", toString(encodedId)); 
    // save to mongodb
    const matchId = encodedId.and("0xffffffffffff");
    console.log("matchId", toString(matchId));
    const betId = toString(id);
    const _odd = odd.div(10000);
    const gameId = encodedId.shr(48);
    console.log("GameId", toString(gameId));
    const { DOMAIN, SUB_DOMAIN, endpoints } = constants.apiConfig;
    const url = `${DOMAIN}${SUB_DOMAIN}${endpoints.SAVE_HISTORY}`;
    const configs = {
        method: constants.apiConfigs.method.POST,
        url: url,
        data: {
            matchId: toString(matchId),
            user: user,
            side: toString(side),
            odd: toString(_odd),
            settleStatus: toString(settleStatus),
            amount: _amount,
            betId: toString(betId),
        },
    };
    console.log("configs", configs);
    const result = await (await axios(configs)).data;
    console.log("result", result);
};
const handleHistoryResolveMatchAction: ActionFn = async (
    context: Context,
    event: Event
) => {
    let txEvent = event as TransactionEvent;
    const iface = new ethers.utils.Interface(constants.BET_TO_WIN_ABI);
    const length = txEvent.logs.length;
    const eventDecoded = iface.decodeEventLog(
        "MatchResolved",
        txEvent.logs[length - 1].data,
        txEvent.logs[length - 1].topics
    );
    const { gameId, matchId, status } = eventDecoded;
    console.log("Event Emitted", {
        matchId: toString(matchId),
        gameId: toString(gameId),
        status: toString(status),
    });
    // save to mongodb

    const { DOMAIN, SUB_DOMAIN, endpoints } = constants.apiConfig;
    const url = `${DOMAIN}${SUB_DOMAIN}${endpoints.UPDATE_HISTORY}`;
    const configs = {
        method: constants.apiConfigs.method.POST,
        url: url,
        data: {
            matchId: toString(matchId),
            isSolved: true,
        },
    };
    console.log("configs", configs);
    await axios(configs);
};

const handleHistorySettleBetAction: ActionFn = async (
    context: Context,
    event: Event
) => {
    let txEvent = event as TransactionEvent;
    const iface = new ethers.utils.Interface(constants.BET_TO_WIN_ABI);
    const eventDecoded = iface.decodeEventLog(
        "BetSettled",
        txEvent.logs[0].data,
        txEvent.logs[0].topics
    );
    const { id, to, referree } = eventDecoded;
    console.log("Event Emitted", {
        matchId: toString(id),
        to: to,
        referree: referree,
    });
    // save to mongodb

    const { DOMAIN, SUB_DOMAIN, endpoints } = constants.apiConfig;
    const url = `${DOMAIN}${SUB_DOMAIN}${endpoints.UPDATE_HISTORY}`;
    const configs = {
        method: constants.apiConfigs.method.POST,
        url: url,
        data: {
            matchId: toString(id),
            to: to,
            isPaid: true,
        },
    };
    console.log("configs", configs);
    await axios(configs);
};
const resolveMatchAction: ActionFn = async (context: Context, event: Event) => {
    console.log("Action resolve match");
    const txEvent = event as WebhookEvent;
    const {matchData} = txEvent.payload;
    const match: Match = matchData as Match;
    const croupierPK = await context.secrets.get("CROUPIER_PK");
    await handleMatchFinished(match, croupierPK);
}
export {
    betPlacedAction,
    checkMatchFinishedAction,
    reCheckMatchAction,
    saveBetPlacedHistoryAction,
    handleHistoryResolveMatchAction,
    handleHistorySettleBetAction,
    resolveMatchAction,
};
