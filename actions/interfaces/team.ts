interface Team {
    id?: number;
    name?: string;
    logo?: string;
    winner?: boolean;
}
export default Team;