interface Status {
    idx?: number;
    code?: string;
    name?: string;
    description?: string;
    elapsed?: number | null;
}
export default Status;