import Team from './team';
import League from './league';
import Score from './score';
import Status from './status';
interface Match {
    id?: number;
    referee: string;
    timezone?: string;
    date?: string;
    timestamp?: number;
    period?: {
        first?: number;
        second?: number;
    },
    venue?: {
        id?: number;
        name?: string;
        city?: string;
    },
    status?: Status ;
    match_start?: string;
    league?: League;
    teams?: {
        home?: Team;
        away?: Team;
    }
    goals?: Score,
    score?: {
        halftime?: Score;
        fulltime?: Score;
        extratime?: Score;
        penalty?: Score;
    },
    events?: any;
    lineups?: any;
    statistics?: any;
    players?: any;
}
export default Match;