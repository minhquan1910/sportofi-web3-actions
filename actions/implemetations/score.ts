import Score from "../interfaces/score";
class ScoreImpl {
    public from(obj: any): Score {
        const score: Score = {
            home: obj?.home ?? 0,
            away: obj?.away ?? 0,
        };
        return score;
    }
}
export default ScoreImpl;