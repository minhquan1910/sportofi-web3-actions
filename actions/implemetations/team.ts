import { Team } from "../interfaces";
class TeamImpl {
    public from(obj: any): Team {
        const team: Team = {
            id: obj?.id ?? 0,
            name: obj?.name ?? "",
            logo: obj?.logo ?? "",
            winner: obj?.winner ?? false,
        };
        return team;
    }
}
export default TeamImpl;