import { Match } from "../interfaces";
import constants from "../constants";
import LeagueImpl from "./league";
import TeamImpl from "./team";
import ScoreImpl from "./score";
import axios from "axios";
import StatusCodeImpl from "./statusCode";
class MatchImpl {
	public from(obj: any): Match {
		const match: Match = {
        	id: obj?.fixture?.id ?? 0,
        	referee:  obj?.fixture?.referee ?? "",
        	timezone: obj?.fixture?.timezone ?? "",
        	date: obj?.fixture?.date ?? "",
        	timestamp: obj?.fixture?.timestamp ?? 0,
        	period: {
            	first: obj?.fixture?.periods?.first ?? 0,
            	second: obj?.fixture?.periods?.second ?? 0,
        	},
        	venue: {
            	id: obj?.fixture?.venue?.id ?? 0,
            	name: obj?.fixture?.venue?.name ?? "",
            	city: obj?.fixture?.venue?.city ?? "",
        	},
        	status: new StatusCodeImpl().from(obj?.fixture?.status?.short ?? "", obj?.fixture?.status?.elapsed ?? 0),
        	match_start: obj?.fixture?.date ?? "",
        	league: new LeagueImpl().from(obj?.league),
        	teams: {
            	home: new TeamImpl().from(obj?.teams?.home),
            	away: new TeamImpl().from(obj?.teams?.away),
        	},
        	goals: new ScoreImpl().from(obj?.goals),
        	score: {
            	halftime: new ScoreImpl().from(obj?.score?.halftime),
            	fulltime: new ScoreImpl().from(obj?.score?.fulltime),
            	extratime: new ScoreImpl().from(obj?.score?.extratime),
            	penalty:  new ScoreImpl().from(obj?.score?.penalty),
        	},
        	events: obj?.events,
        	lineups: obj?.lineups,
        	statistics: obj?.statistics,
        	players: obj?.players,
    	};
    	return match;
	}
    public static getMatchById = async (matchId: number, apikey: string): Promise<Match> => {   
		const url = `${constants.apiConfigs.DOMAIN_NAME}${constants.apiConfigs.endpoints.FIXTURES}?id=${matchId}`;
		const configs = {
  			method: constants.apiConfigs.method.GET,
  			url: url,
  			headers: { 
    			[constants.apiConfigs.headers.X_RAPID_API_HOST]: constants.apiConfigs.API_HOST, 
    			[constants.apiConfigs.headers.X_RAPID_API_KEY]: apikey
  			}
		};
		console.log("configs", configs);
		const result = await (await axios(configs)).data;
		const data = result?.response?.[0] ?? {};
		const match: Match = new MatchImpl().from(data);
	    return match;
    }
    public static getMatchByIds = async (matchIdString: string, apikey: string): Promise<Match[]> => {  
		let url = `${constants.apiConfigs.DOMAIN_NAME}${constants.apiConfigs.endpoints.FIXTURES}?ids=${matchIdString}`;
		const configs = {
  			method: constants.apiConfigs.method.GET,
  			url: url,
  			headers: { 
    			[constants.apiConfigs.headers.X_RAPID_API_HOST]: constants.apiConfigs.API_HOST, 
    			[constants.apiConfigs.headers.X_RAPID_API_KEY]: apikey
  			}
		};
		console.log("configs", configs);
		const result = await (await axios(configs)).data;
		const matchs: Match[] = result?.response ? result?.response?.map((item: any) => new MatchImpl().from(item)) : [];
	    return matchs;
    }

	public static getSideInFavor = (match: Match): number | null => {
		console.log("Get Side In Favor")
		if(this.isMatchFinished(match?.status?.code)) {
			const homeScore = match?.score?.fulltime?.home ?? 0;
			console.log("Home Score", homeScore);
			const awayScore = match?.score?.fulltime?.away ?? 0;
			console.log("awayScore", awayScore);
			if (homeScore > awayScore) {
				console.log("Home Win")
				return constants.sides.HOME;
			} else if (homeScore < awayScore) {
				console.log("Away Win")
				return constants.sides.AWAY;
			} else {
				console.log("Draw")
				return constants.sides.DRAW;
			}
		}
		return null;
	}
	public static isMatchFinished = (code?: string): boolean => {
		let isFinished: boolean;
		switch (code) {
			case constants.matchStatuses.MATCH_FINISHED.code:
				isFinished = true;
				break;
			case constants.matchStatuses.MATCH_FINISHED_AFTER_EXTRA_TIME.code:
				isFinished = true;
				break;
			case constants.matchStatuses.MATCH_FINISHED_AFTER_PENALTY.code:
				isFinished = true;
			default:
				isFinished = false;
				break;
		}
		return isFinished;
	}
}
export default MatchImpl;