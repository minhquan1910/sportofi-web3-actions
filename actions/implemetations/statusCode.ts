import constants from '../constants'
import { Match, Status } from '../interfaces';
const moment = require('moment-timezone');
class StatusCodeImpl {
    public from(statusCode: string, elapsed: number): Status {
        const status: Status | null = this.getStatusByCode(statusCode, elapsed);
        return status ? status : {};
    }

    public static getPeriodic (match?: Match): string {
        const status = match?.status
        const {DAILY, EVERY_5_MINUTES} = constants.storage.periodic;
        const {TIME_TO_BE_DEFINED, POSTPONED, CANCELED, NOT_STARTED } = constants.matchStatuses;
        let periodic:string = '';
        switch (status?.code) {
            case TIME_TO_BE_DEFINED.code:
                periodic = DAILY
                break;
            case POSTPONED.code:
                periodic = DAILY
                break;
            case CANCELED.code:
                periodic = DAILY
                break;
            case NOT_STARTED.code:
                const matchStart = moment(match?.match_start).tz('UTC');
                const tomorrow = moment().tz('UTC').add(1,'day');
                if(tomorrow <= matchStart){
                    periodic = DAILY
                }
                else{
                    periodic = EVERY_5_MINUTES
                }
                console.log("Not started:", {
                    matchStart: matchStart.format(),
                    tomorrow: tomorrow.format(),
                    periodic
                })
                break;
            default:
                periodic = EVERY_5_MINUTES;    
                break;
        }
        return periodic;
    }

    private getStatusByCode = (code?: string, elapsed?: number): Status | null => {
        if (code !== undefined) {
            const matchStatuses: any = constants.matchStatuses;
            for (const key in matchStatuses) {
                const status: Status = matchStatuses[key];
                if (status.code === code) {
                    status.elapsed = elapsed
                    return status;
                }
            }
        }
        return null;
    }
}
export default StatusCodeImpl;