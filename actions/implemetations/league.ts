import League from "../interfaces/league";
class LeagueImpl {
    public from(obj: any): League {
        const league: League = {
            id: obj?.id ?? 0,
            name: obj?.name ?? "",
            country: obj?.country ?? "",
            logo: obj?.logo ?? "",
            flag: obj?.flag ?? "",
            season: obj?.season ?? 0,
            round: obj?.round ?? "",
        };
        return league;
    }
}
export default LeagueImpl;